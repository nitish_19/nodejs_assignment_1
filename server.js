const http = require('http');
const fs = require('fs');

let lineNumber = 0;
const requestHandler = (req, res) => {
    if (req.method == "GET") {
        lineNumber++;
        fs.readFile('text.json', 'utf8', (err, data) => {
            if (err) {
                res.writeHead(403, { "Content-Type": "application/json" });
                res.end(`{ "message": ${err} }`);
                return;
            }

            // parsing invalid JSON data can throw error
            try {
                const user = JSON.parse(data);
                const userDetails = `${lineNumber}). ${user.name} Details: \n    The name of user is ${user.name}, he is ${user.age} years old. He is currently in ${user.department}, Gender is ${user.gender}. \n\n`;

                fs.appendFile('data.txt', userDetails, err => {
                    if (err) {
                        res.writeHead(403, { "Content-Type": "application/json" });
                        res.end(`{ "message": ${err.message} }`);
                    } else {
                        res.writeHead(200, { "Content-Type": "application/json" });
                        res.end(`{"message": "success", "data": ${JSON.stringify(user)}}`);
                    }
                });
            } catch (err) {
                res.writeHead(400, { "Content-Type": "application/json" });
                res.end(`{ "message": ${err} }`);
            }
        });
    } else {
        res.writeHead(403, { "Content-Type": "application/json" });
        res.end(`{ "message": "Invalid Request" }`);
    }
}

const server = http.createServer(requestHandler);

server.listen(8080);