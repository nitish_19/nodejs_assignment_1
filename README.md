# NodeJS Assignment 1

Creating server using http module and applying read/write operations on files.

# Running project

1). Clone the gitlab repo using : git clone https://gitlab.com/nitish_19/nodejs_assignment_1

2). Go to directory where server.js is located and run command: node server.js

3). Your http server has started now, start making request through Postman.